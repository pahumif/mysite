


from flask import Flask, render_template, request, redirect, url_for
from pony.orm import Database, Required, Optional, PrimaryKey, select, db_session
from person import Person


app = Flask(__name__)
db = Database()


class Todo(db.Entity):
    id = PrimaryKey(int,auto=True)
    task_name = Required(str)
    duration = Required(int)
    location = Optional(str)

class Product(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    quantity = Required(int)
    price = Required(int)


db.bind(provider='sqlite', filename='productdb', create_db=True)
db.generate_mapping(create_tables=True)


@app.route('/products', methods=['GET','POST'])
@db_session
def products():

    if request.method == 'GET':

        searched_term = request.args.get('searched_term','')
        products = select(p for p in Product if searched_term.upper() in p.name.upper())

        return render_template('product_table.html', PRODUCTS=products)

    elif request.method == 'POST':
        name = request.form.get('name')
        quantity = request.form.get('quantity')
        price = request.form.get('price')

        Product(name=name, quantity=quantity, price=price)

        products = select(p for p in Product if searched_term in p.name)

        return render_template('product_table.html', PRODUCTS=products)


@app.route('/products/delete/<int:id>')
@db_session
def delete_product(id):
    if Product[id]:
        Product[id].delete()

    return redirect(url_for('products'))


@app.route('/products/<id>')
@db_session
def product_details(id):

    if request.method == 'GET':
        if Product[id]:
            return render_template('product_modify.html', PRODUCT=Product[id])

    elif request.method == 'POST':
        if Product[id]:
            n = request.form.get('Name')
            q = request.form.get('Quantity')
            p = request.form.get('Price')

            Product[id].set(name=n, quantity=q, price=p)

            return redirect(url_for('products'))


@app.route('/todo2', methods=['GET','POST'])
@db_session
def todo2():

    if request.method == 'GET':

        new_things = list(select(t for t in Todo))

        return render_template('todo.html', TODO=new_things)

    elif request.method == 'POST':

        task_name = request.form.get('task_name')
        duration = request.form.get('duration')
        location = request.form.get('location')

        Todo(task_name=task_name, duration=duration, location=location)

        return render_template('todo.html', TODO=new_things)




@app.route('/')
def index():
    return render_template('index.html')


@app.route('/profile')
def profile():
    return 'Welcome to the profile page'


@app.route('/table')
def table():

    people = [Person('Era', 'student', 'Tirana'),
            Person('Eri', 'student', 'Durres'),
            Person('Ana', 'teacher', 'Elbasan'),
            Person('Eni', 'economist', 'Vlora'),
            Person('Sara', 'student', 'Durres')]

    return render_template('table.html', PEOPLE=people)


@app.route('/todo', methods=['GET','POST'])
def todo():

    things = [{'task_name':'eat','duration':'10','location':'home'},
                {'task_name':'sleep','duration':'100','location':'home'}]

    if request.method == 'GET':
        return render_template('todo.html', TODO=things)
    elif request.method == 'POST':
        new_dict = {'task_name': request.form.get('task_name','DEFAULT_TASK'),
                    'duration': request.form.get('duration', 10),
                    'location': request.form.get('location_name','DEFAULT_LOCATION')}
        things.append(new_dict)
        return render_template('todo.html', TODO=things)


if __name__ == '__main__':
    app.run(threaded=True, port=5000)




